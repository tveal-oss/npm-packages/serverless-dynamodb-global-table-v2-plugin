# Sample DynamoDB Global Table V2 via Serverless

Steps to deploy sample table to your active AWS account
1. Install dependencies (ci for version-locked deps)
    ```bash
    npm ci
    ```
2. Deploy to primary region (configured: east)
    ```bash
    npm run dp:sandbox:e
    ```
3. Deploy to replica region (configured: west)
    ```bash
    npm run dp:sandbox:w
    ```
